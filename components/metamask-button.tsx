import { useAddress, useDisconnect, useMetamask } from "@thirdweb-dev/react";
import { Button } from '@chakra-ui/react'
function  ConnectMetamaskButton() {
  const connectWithMetamask = useMetamask();
  const address = useAddress();
  return (
    <div>
      {address ? (
        <h4>あなたのウォレットアドレスは {address}</h4>
      ) : (
        <Button onClick={connectWithMetamask} colorScheme='blue'>Metamaskにつなぐ</Button>
      )}
    </div>
  );
};
export default ConnectMetamaskButton;