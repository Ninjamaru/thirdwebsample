import { ChakraProvider } from '@chakra-ui/react'
import type { AppProps } from 'next/app'
import { ChainId, ThirdwebProvider } from "@thirdweb-dev/react";



function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThirdwebProvider desiredChainId={ChainId.Mainnet}>
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
    </ThirdwebProvider>
  )
}

export default MyApp